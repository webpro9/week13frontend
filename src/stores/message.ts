import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const message = ref("");

  function showError(text: string) {
    message.value = text;
    isShow.value = true;
  }
  return { isShow, message, showError };
});
