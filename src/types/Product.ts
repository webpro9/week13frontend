export default interface Product {
  id?: number;
  name: string;
  price: number;
  createdate?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
