export default interface User {
  id?: number;
  name: string;
  age: number;
  createdate?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
